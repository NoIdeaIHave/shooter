﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneScrolling : MonoBehaviour {

	private bool isMoving;

	void Start () {
		isMoving = true;
	}
	
	void Update () {
		if (Input.GetKey (KeyCode.P)) {
			isMoving = true;
		}

		if (isMoving) {
			transform.position += transform.forward * -30 * Time.deltaTime;
		}
	}


	public void StartMoving(){
		isMoving = true;
	}

	public void StopMoving(){
		isMoving = false;
	}

}
