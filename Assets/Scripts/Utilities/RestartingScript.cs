﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartingScript : MonoBehaviour {

	private bool isPlayerDead;
	void Start () {
		isPlayerDead = false;
	}
	
	void Update () {

		if (isPlayerDead) {
			if (Input.GetKeyDown (KeyCode.R))
            {
                SceneManager.LoadScene(0);
			}
		}
	}

	public void PlayerDead ()
	{
		isPlayerDead = true;
	}
}
