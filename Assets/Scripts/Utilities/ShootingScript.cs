﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour {

	public GameObject bullet;
	public Transform shotSpawn;

	private float fireRate;
	private float nextFire;
	void Start () {
		fireRate = 0.5f;
	}
	
	void Update () {

		StartCoroutine (Shoot());
	}

	private IEnumerator Shoot(){
		if (Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate (bullet, shotSpawn.position, shotSpawn.rotation);
		}
		yield return new WaitForSeconds(1);
	}
}
