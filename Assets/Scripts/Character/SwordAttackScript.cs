﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttackScript : MonoBehaviour
{
	private float attackRate;
	private float nextAttack;
	private float 
	private List<GameObject> enemies;

	// Use this for initialization
	void Start () {
		attackRate = 2f;
	    enemies = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Attack()
    {
		if (Time.time > nextAttack) {
			nextAttack = Time.time + attackRate;
			Debug.Log (enemies);
		}
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy" && !enemies.Contains(other.gameObject))
        {
            Debug.Log("Coś przyszło");
            enemies.Add(other.gameObject);
        }

    }

    void OnTriggerExit(Collider other)
    {
        
        if (enemies.Contains(other.gameObject) && other.gameObject.tag == "Enemy")
        {
            Debug.Log("coś wyszło");
            enemies.Remove(other.gameObject);
        }
    }
}
