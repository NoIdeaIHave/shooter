﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float XMin, XMax, ZMin, ZMax;
}

public class PlayerAction : MonoBehaviour
{
    public Boundary boundary;
    public PlaneScrolling planeScrolling;
    public RestartingScript restartingScript;
    public PlayerInfo playerInfo;

    public Transform playerShotSpawn;
    public GameObject bullet;
    public SwordAttackScript SwordAttackScript;

    private float fireRate;
    private float nextFire;
    private float nextAttack;
    private float attackRate;
    void Start()
    {
        fireRate = 0.5f;
        attackRate = 1.5f;
    }

    void FixedUpdate()
    {
        RotatePlayer();
        MovePlayer();

        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0))
        {
            Shoot();
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
			SwordAttackScript.Attack();
        }
    }

    private void MovePlayer()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        gameObject.transform.position += movement * 3;

        gameObject.transform.position = new Vector3
        (
            Mathf.Clamp(gameObject.transform.position.x, boundary.XMin, boundary.XMax),
            0.0f,
            Mathf.Clamp(gameObject.transform.position.z, boundary.ZMin, boundary.ZMax)
        );
    }

    private void Shoot()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            var playerBullet = Instantiate(bullet, playerShotSpawn.position, playerShotSpawn.rotation);
            playerBullet.gameObject.name = "Player bullet";
            playerBullet.gameObject.tag = "Player bullet";
        }
    }

    private void RotatePlayer()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.y = 0;
        Quaternion lookRotation = Quaternion.LookRotation(gameObject.transform.position - mousePosition);
        gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, lookRotation, 5);
    }

    private void Attack()
    {
        if (Time.time > nextAttack)
        {
            
        }

            Debug.Log("Just Attacked!");
    }


    void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Bullet":

                Destroy(other.gameObject);
                var bulletScript = other.gameObject.GetComponent("BulletScript") as BulletScript;
                playerInfo.AddjustHealth(-bulletScript.Damage);
                break;

            case "HealthPack":

                Destroy(other.gameObject);
                var healthPackScript = other.gameObject.GetComponent("HealthPackScript") as HealthPackScript;
                playerInfo.AddjustHealth(healthPackScript.Health);
                break;
        }
    }
}