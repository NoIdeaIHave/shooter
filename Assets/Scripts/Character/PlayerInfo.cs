﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public float MaxHealth = 100;
    public float Health = 100;
    public int Level = 1;

    public float healthBarLength;

    void Start()
    {
        healthBarLength = Screen.width / 2;
    }

    void OnGUI()
    {
        GUI.Box(new Rect(200, 530, healthBarLength, 20), Health + "/" + MaxHealth);
    }
    

    public void AddjustHealth(float damage)
    {
        Health += damage;
        if(Health < 1)
        {
            Health = 0;
            Destroy(gameObject);
        }
        if(Health > MaxHealth)
        {
            Health = MaxHealth;
        }

        healthBarLength = (Screen.width / 2) * (Health / MaxHealth);
    }

    public void LevelUp()
    {
        Level++;
    }

    public void LevelUp(int levelNumber)
    {
        Level = Level + levelNumber;
    }
}
