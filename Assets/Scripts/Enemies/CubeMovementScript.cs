﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovementScript : MonoBehaviour {

	public Transform PlayerTransform;
	public int RotationSpeed;

	private Transform _cubeTransform;
	void Awake(){
		_cubeTransform = transform;
	}

	void Start () {
	}
	
	void Update () {
		if (PlayerTransform != null) {

			Quaternion lookRotation = Quaternion.LookRotation (PlayerTransform.position - _cubeTransform.position);

			_cubeTransform.rotation = Quaternion.Slerp (_cubeTransform.rotation, lookRotation , RotationSpeed * Time.deltaTime);
		}
	}
}
